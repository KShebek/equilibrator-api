.. equilibrator documentation master file, created by
   sphinx-quickstart on Mon Jan  4 20:45:45 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

eQuilibrator overview
=====================
``equilibrator-api`` is a command-line API with minimal dependencies for calculation of standard 
thermodynamic potentials of biochemical reactions using the same data found on 
`eQuilibrator website <http://equilibrator.weizmann.ac.il/>`_.
It can be used without a network connection (after installation and initialization).


Current Features
****************
* Calculation of standard Gibbs potentials of reactions (together with confidence intervals).
* Calculation of standard reduction potentials of half-cells.
* Adjustment of Gibbs free energies to pH, ionic strength, and pMg (Magnesium ion concentration).
* Pathway analysis tools such as Max-min Driving Force and Enzyme Cost Minimization (requires the `equilibrator-pathway <https://gitlab.com/equilibrator/equilibrator-pathway>`_ package).
* Adding new compounds that are not among the 500,000 currently in the MetaNetX database (requires the `equilibrator-assets <https://gitlab.com/equilibrator/equilibrator-assets>`_ package).


How to cite us
**************
If you plan to use results from ``equilibrator-api`` in a scientific publication,
please cite our paper: `Consistent Estimation of Gibbs Energy Using Component Contributions` :footcite:`noor_consistent_2013`


How to install
**************
You can simply ``pip install equilibrator-api`` and start eQuilibrating. For more details, see :ref:`Installation`.


How to use
**********
You can find a simple example in the :ref:`Example Usage` section. Or take a look at the :ref:`API reference` for a full list of classes and functions.

.. toctree::
   :hidden:
   :numbered:
   :maxdepth: 2

   self
   installation
   equilibrator_examples.ipynb
   autoapi/index


.. image:: _static/may_the_driving_force_be_with_you.svg

References
**********

.. footbibliography::

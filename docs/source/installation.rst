Installation
============

Step 1
******
The easiest way to get eQuilibrator-API up and running is using virtualenv, PyPI, and Jupyter notebooks

.. code-block:: python

    virtualenv -p python3 equilibrator
    source equilibrator/bin/activate
    pip install equilibrator-api jupyter


If you are using a Windows environment, we recommend using `conda` instead of `pip`:


.. code-block:: python

    conda install -c conda-forge equilibrator-api


Step 2 (optional)
*****************
Run this command to initialize eQuilibrator:

.. code-block:: python

    python -c "from equilibrator_api import ComponentContribution; cc = ComponentContribution()"


Note, that this can take minutes or even up to an hour, since about 1.3 GBytes of data need to be downloaded from a remote website (`Zenodo <https://zenodo.org/>`_). If this command fails, try improving the speed of your connection (e.g. disabling your VPN, or using a LAN cable to connect to your router) and running it again.

Note that you don't have to run this command before using eQuilibrator. It will simply download the database on the first time you try using it (e.g. inside the Jupyter notebook). In any case, after downloading the database the data will be locally cached and loading takes only a few seconds from then onwards.


Step 3 (optional)
*****************
Now, you are good to go. In case you want to see an example of how to use eQuilibrator-API in the form of a Jupyter notebook, run the following commands:

.. code-block:: python

    curl https://gitlab.com/equilibrator/equilibrator-api/-/raw/develop/scripts/equilibrator_cmd.ipynb > equilibrator_cmd.ipynb
    jupyter notebook

and select the notebook called `equilibrator_cmd.ipynb` and follow the examples in it.


Dependencies
************

- python >= 3.6
- equilibrator-cache
- component-contribution
- numpy
- scipy
- pandas
- python-Levenshtein-wheels
- pint
- path
- appdirs
- diskcache
- httpx
- tenacity
- periodictable
- uncertainties
- pyzenodo3
- python-slugify
- cobra (optional)


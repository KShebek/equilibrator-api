# The MIT License (MIT)
#
# Copyright (c) 2019 Novo Nordisk Foundation Center for Biosustainability,
# Technical University of Denmark.
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.


"""Test that COBRA reactions are translated correctly."""


from pathlib import Path

import cobra
import pytest
from cobra.io import read_sbml_model

from equilibrator_api import compatibility as compat


@pytest.fixture(scope="module")
def core_model() -> cobra.Model:
    """Return the E. coli core model."""
    model_path = Path(__file__).parent / "data" / "e_coli_core.xml.gz"
    return read_sbml_model(str(model_path))


@pytest.mark.parametrize(
    "reaction_ids, expected", [([], 0), (["PDH", "PFK"], 2)]
)
def test_map_cobra_reactions(
    comp_contribution, core_model, reaction_ids, expected
):
    """Expect that reactions can be mapped."""
    reactions = core_model.reactions.get_by_any(reaction_ids)
    mapping = compat.map_cobra_reactions(
        comp_contribution.ccache,
        reactions,
        annotation_preference=("inchi_key",),
        inchi_key_namespace="inchi_key",
    )
    assert len(mapping) == expected


@pytest.mark.parametrize(
    "reaction_id, expected",
    [
        (
            "PDH",
            "metanetx.chemical:MNXM23 + metanetx.chemical:MNXM12 + "
            "metanetx.chemical:MNXM8 = metanetx.chemical:MNXM21 + "
            "metanetx.chemical:MNXM13 + metanetx.chemical:MNXM10",
        ),
        (
            "PFK",
            "metanetx.chemical:MNXM3 + metanetx.chemical:MNXM89621 = "
            "metanetx.chemical:MNXM7 + metanetx.chemical:MNXM417 + "
            "metanetx.chemical:MNXM1",
        ),
    ],
)
def test_map_cobra_model(comp_contribution, core_model, reaction_id, expected):
    """Expect that reactions can be mapped."""
    reactions = core_model.reactions.get_by_any([reaction_id])
    mapping = compat.map_cobra_reactions(
        comp_contribution.ccache,
        reactions,
        annotation_preference=("inchi_key",),
        inchi_key_namespace="inchi_key",
    )
    assert len(mapping) == 1
    rxn = core_model.reactions.get_by_id(reaction_id)
    expected_reaction = comp_contribution.parse_reaction_formula(expected)
    assert {m.inchi_key for m in mapping[rxn.id].keys()} == {
        m.inchi_key for m in expected_reaction.keys()
    }

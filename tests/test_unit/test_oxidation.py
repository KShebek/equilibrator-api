"""unit test for balance with oxidation function."""
# The MIT License (MIT)
#
# Copyright (c) 2013 Weizmann Institute of Science
# Copyright (c) 2018 Institute for Molecular Systems Biology,
# ETH Zurich
# Copyright (c) 2018 Novo Nordisk Foundation Center for Biosustainability,
# Technical University of Denmark
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

import pytest


@pytest.mark.parametrize(
    "compound_id, exp_standard_dg_prime",
    [
        ("kegg:C00031", -2927.8),
        ("kegg:C00033", -871.1),
        ("kegg:C00064", -2040.3),
    ],
)
def test_oxidation(compound_id, exp_standard_dg_prime, comp_contribution):
    """Test how reactions are balanced by oxidation."""
    compound = comp_contribution.get_compound(compound_id)
    r1 = comp_contribution.get_oxidation_reaction(compound)
    dg_prime1 = comp_contribution.dg_prime(r1)
    assert dg_prime1.value.m_as("kJ/mol") == pytest.approx(
        exp_standard_dg_prime, abs=0.1
    )

    reaction = comp_contribution.parse_reaction_formula(f"{compound_id} = ")
    r2 = comp_contribution.balance_by_oxidation(reaction)
    dg_prime2 = comp_contribution.dg_prime(r2)
    assert dg_prime2.value.m_as("kJ/mol") == pytest.approx(
        exp_standard_dg_prime, abs=0.1
    )

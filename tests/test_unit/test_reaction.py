"""unit test for PhaseReaction and ComponentContribution."""
# The MIT License (MIT)
#
# Copyright (c) 2013 Weizmann Institute of Science
# Copyright (c) 2018 Institute for Molecular Systems Biology,
# ETH Zurich
# Copyright (c) 2018 Novo Nordisk Foundation Center for Biosustainability,
# Technical University of Denmark
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.


import warnings

import numpy
import pytest

from equilibrator_api import Q_


def test_water_balancing(reaction_dict, comp_contribution):
    """Test if the reaction can be balanced using H2O."""
    missing_water = reaction_dict["missing_water"]
    assert not missing_water.is_balanced()
    assert missing_water.is_balanced(ignore_atoms=("H", "O", "e-"))

    balanced_rxn = missing_water.balance_with_compound(
        comp_contribution.get_compound("kegg:C00001"),
        ignore_atoms=("H",),
    )
    assert balanced_rxn is not None


def test_atp_hydrolysis_physiological_dg(reaction_dict, comp_contribution):
    """Test the dG adjustments for physiological conditions (with H2O)."""
    warnings.simplefilter("ignore", ResourceWarning)

    atp_hydrolysis = reaction_dict["atpase"]
    assert atp_hydrolysis.is_balanced()

    phys_dg_corr = atp_hydrolysis.physiological_dg_correction()
    assert phys_dg_corr.m_as("") == pytest.approx(numpy.log(1e-3), rel=1e-3)

    for compound_id, concentration in [
        ("kegg:C00002", "1uM"),
        ("kegg:C00008", "1uM"),
        ("kegg:C00009", "1uM"),
    ]:
        atp_hydrolysis.set_abundance(
            comp_contribution.get_compound(compound_id),
            Q_(concentration),
        )

    dg_corr = atp_hydrolysis.dg_correction()
    assert dg_corr.m_as("") == pytest.approx(numpy.log(1e-6), rel=1e-3)


def test_fermentation_gas(reaction_dict):
    """Test the dG adjustments for physiological conditions (in gas phase)."""
    fermentation_gas = reaction_dict["fermentation_gas"]
    assert fermentation_gas.is_balanced()
    assert fermentation_gas.physiological_dg_correction().m_as(
        ""
    ) == pytest.approx(numpy.log(1e-9), rel=1e-3)


def test_multi(comp_contribution, reaction_dict):
    """Test the dG estimate for mutli-reactions."""
    (
        standard_dg_prime,
        dg_uncertainty,
    ) = comp_contribution.standard_dg_prime_multi(
        [
            reaction_dict["fermentation_gas"],
            reaction_dict["pyruvate_decarboxylase"],
        ],
        uncertainty_representation="cov",
    )
    assert standard_dg_prime[0].m_as("kJ/mol") == pytest.approx(-214.8, abs=0.1)
    assert standard_dg_prime[1].m_as("kJ/mol") == pytest.approx(-16.6, abs=0.1)

    assert dg_uncertainty[0, 0].m_as("kJ**2/mol**2") == pytest.approx(
        42.5, abs=0.1
    )
    assert dg_uncertainty[0, 1].m_as("kJ**2/mol**2") == pytest.approx(
        20.6, abs=0.1
    )
    assert dg_uncertainty[1, 0].m_as("kJ**2/mol**2") == pytest.approx(
        20.6, abs=0.1
    )
    assert dg_uncertainty[1, 1].m_as("kJ**2/mol**2") == pytest.approx(
        10.7, abs=0.1
    )


def test_atp_hydrolysis_dg(comp_contribution, reaction_dict):
    """Test the CC predictions for ATP hydrolysis."""
    warnings.simplefilter("ignore", ResourceWarning)
    atp_hydrolysis = reaction_dict["atpase"]

    for compound_id, concentration in [
        ("kegg:C00002", "1mM"),
        ("kegg:C00008", "10mM"),
        ("kegg:C00009", "10mM"),
    ]:
        atp_hydrolysis.set_abundance(
            comp_contribution.get_compound(compound_id),
            Q_(concentration),
        )

    standard_dg_prime = comp_contribution.standard_dg_prime(atp_hydrolysis)

    assert standard_dg_prime.value.m_as("kJ/mol") == pytest.approx(
        -30.1, abs=0.1
    )
    assert standard_dg_prime.error.m_as("kJ/mol") == pytest.approx(0.3, abs=0.1)

    dg_prime = comp_contribution.dg_prime(atp_hydrolysis)
    assert dg_prime.value.m_as("kJ/mol") == pytest.approx(-35.8, abs=0.1)

    physiological_dg_prime = comp_contribution.physiological_dg_prime(
        atp_hydrolysis
    )
    assert physiological_dg_prime.value.m_as("kJ/mol") == pytest.approx(
        -47.2, abs=0.1
    )


def test_gibbs_energy_pyruvate_decarboxylase(comp_contribution, reaction_dict):
    """Test the CC predictions for pyruvate decarboxylase."""
    warnings.simplefilter("ignore", ResourceWarning)

    reaction = reaction_dict["pyruvate_decarboxylase"]

    assert reaction.is_balanced()

    standard_dg_prime = comp_contribution.standard_dg_prime(reaction)
    assert standard_dg_prime.value.m_as("kJ/mol") == pytest.approx(
        -16.6, abs=0.1
    )
    assert standard_dg_prime.error.m_as("kJ/mol") == pytest.approx(3.3, abs=0.1)


def test_reduction_potential(comp_contribution, reaction_dict):
    """Test the CC predictions for a redox half-reaction."""
    warnings.simplefilter("ignore", ResourceWarning)

    reaction = reaction_dict["oxaloacetate_half"]

    assert reaction.check_half_reaction_balancing() == 2
    assert reaction.is_balanced(ignore_atoms=["H", "e-"])

    standard_e_prime = comp_contribution.standard_e_prime(reaction)

    assert standard_e_prime.value.m_as("mV") == pytest.approx(-175.7, abs=0.1)
    assert standard_e_prime.error.m_as("mV") == pytest.approx(3.3, abs=0.1)


def test_unresolved_reactions(comp_contribution, reaction_dict):
    """Test the CC predictions for a reaction that cannot be resolved."""
    reaction = reaction_dict["unresolved"]
    standard_dg_prime = comp_contribution.standard_dg_prime(reaction)
    assert standard_dg_prime.error.m_as("kJ/mol") > 1e4


def test_generic_compounds(comp_contribution, reaction_dict):
    """Test the CC predictions for reaction that contain generic compounds."""
    reaction = reaction_dict["unresolved"]
    standard_dg = comp_contribution.standard_dg(reaction)
    standard_dg_prime = comp_contribution.standard_dg_prime(reaction)
    assert standard_dg.value.m_as("kJ/mol") == pytest.approx(122.6, abs=0.1)
    assert standard_dg_prime.value.m_as("kJ/mol") == pytest.approx(
        79.7, abs=0.1
    )


def test_reversibility_index(comp_contribution, reaction_dict):
    """Test the reversibility index."""
    atp_hydrolysis = reaction_dict["atpase"]
    ln_gamma = comp_contribution.ln_reversibility_index(atp_hydrolysis)
    assert ln_gamma.value.m_as("") == pytest.approx(-12.7, abs=0.1)


def test_reaction_with_missing_inchi(comp_contribution):
    """Test a reaction where one of the compounds is missing an InChI."""

    rxn_str = "CHEBI:69477 + KEGG:C00007 = CHEBI:69478 + CHEBI:44785"
    rxn = comp_contribution.parse_reaction_formula(rxn_str)
    phys_dg_prime = comp_contribution.physiological_dg_prime(rxn)
    assert phys_dg_prime.error.m_as("kJ/mol") > 1e4
